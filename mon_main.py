﻿#!/usr/bin/python3
""" Ce fichier defini des objets et calcule leur aire et perimetre """
from ma_classe_rectangle import MaClasseRectangle
from ma_classe_cercle import MaClasseCercle
from ma_classe_carre import MaClasseCarre

def le_main():
    """ Instancier les classes et effectue les calculs """
    mon_rectangle = MaClasseRectangle(4, 8, 2, 3)
    mon_carre = MaClasseCarre(10, 5, 6)
    mon_cercle = MaClasseCercle(15, 5, 4)

    # calculer les perimetres et aires
    print("Perimetre Rectangle=" + str(mon_rectangle.calcul_perimetre()))
    print("Aire Rectangle=" + str(mon_rectangle.calcul_aire()))
    print("Perimetre Carre=" + str(mon_carre.calcul_perimetre()))
    print("Aire Carre=" + str(mon_carre.calcul_aire()))
    print("Perimetre Cercle=" + str(mon_cercle.calcul_perimetre()))
    print("Aire Cercle=" + str(mon_cercle.calcul_aire()))

    # deplacer le cercle de +2,+1
    mon_cercle.afficher()
    mon_cercle.deplacer(2, 1)
    mon_cercle.afficher()

if __name__ == "__main__":
    le_main()
