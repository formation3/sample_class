﻿""" Ce fichier decrit la classe forme """

class MaClasseForme():
    """ class Forme """
    _m_pos_x = 0
    _m_pos_y = 0

    def __init__(self, new_x=0, new_y=0):
        """ constructeur """
        self._m_pos_x = new_x
        self._m_pos_y = new_y

    def deplacer(self, delta_x, delta_y):
        """ methode pour deplacer un objet """
        self._m_pos_x += delta_x
        self._m_pos_y += delta_y

    def afficher(self):
        """ Affiche la position de l'objet """
        print("(X=" + str(self._m_pos_x) + ", Y=" + str(self._m_pos_y) + ")")
