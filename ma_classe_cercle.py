﻿""" Ce fichier decrit la classe cercle """
import math
from ma_classe_forme import MaClasseForme

class MaClasseCercle(MaClasseForme):
    """ class cercle """
    _m_rayon = 0

    def __init__(self, new_x=0, new_y=0, le_rayon=0):
        """ constructeur appel le super constructor """
        MaClasseForme.__init__(self, new_x, new_y)
        self._m_rayon = le_rayon

    def calcul_perimetre(self):
        """ Calcule le perimetre """
        return 2 * math.pi * self._m_rayon

    def calcul_aire(self):
        """ Calcule l'aire """
        return math.pi * self._m_rayon * self._m_rayon
