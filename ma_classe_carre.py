""" Ce fichier decrit la classe carre """
from ma_classe_rectangle import MaClasseRectangle

class MaClasseCarre(MaClasseRectangle):
    """ class carre """
    _m_cote = 0

    def __init__(self, new_x=0, new_y=0, le_cote=0):
        """ constructeur appel le super constructor """
        MaClasseRectangle.__init__(self, new_x, new_y)
        self._m_cote = le_cote

    def calcul_perimetre(self):
        """ Calcule le perimetre """
        return 4 * self._m_cote

    def calcul_aire(self):
        """ Calcule l'aire """
        return self._m_cote * self._m_cote
