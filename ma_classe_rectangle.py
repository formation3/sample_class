""" Ce fichier decrit la classe rectangle """
from ma_classe_forme import MaClasseForme

class MaClasseRectangle(MaClasseForme):
    """ class rectangle """
    _m_largeur = 0
    _m_longueur = 0

    def __init__(self, new_x=0, new_y=0, la_longueur=0, la_largeur=0):
        """ constructeur appel le super constructor """
        MaClasseForme.__init__(self, new_x, new_y)
        self._m_longueur = la_longueur
        self._m_largeur = la_largeur

    def calcul_perimetre(self):
        """ Calcule le perimetre """
        return 2 * (self._m_largeur + self._m_longueur)

    def calcul_aire(self):
        """ Calcule l'aire """
        return self._m_largeur * self._m_longueur
